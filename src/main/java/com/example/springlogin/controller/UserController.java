package com.example.springlogin.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.springlogin.controller.bean.User;
import com.example.springlogin.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String welcomePage(ModelMap model, @RequestParam String userId, @RequestParam String password) {

//		if (bindingResult.hasErrors() || bindingResult.hasFieldErrors()) {
//			return "login";
//		}

		System.out.println("user id taken from login page: " + userId);
		System.out.println("password taken from login page: " + password);

		User user = userService.getUserByUserId(userId);

		System.out.println(user);

		System.out.println(user.getPassword());

		if (user.getPassword().equals(password)) {
			System.out.println("Password matches for user");
			model.put("userId", userId);
			return "welcome";
		}

		else {
			System.out.println("Wrong pass");
			model.put("errorMsg", "Please provide correct credentials");
			return "login";
		}

	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerPage(Model model) {
		User user = new User();
		model.addAttribute("user", user);

		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, ModelMap model) {

		if (bindingResult.hasErrors() || bindingResult.hasFieldErrors()) {
			return "register";
		}

		System.out.println(bindingResult);

		String userId = user.getUserId();
		String password = user.getPassword();

		if (userId.length() == 0 || password.length() == 0) {
			model.put("errorMsg", "Fill all fields");
			return "register";

		} else {
			System.out.println("Registered user data: " + user);
		}

		int count = userService.createNewUser(user);

		if (count != 1) {
			model.put("errorMsg", "Error occured");
			return "register";
		}

		model.put("successMsg", "User registered successfully");
		return "login";

	}

}
