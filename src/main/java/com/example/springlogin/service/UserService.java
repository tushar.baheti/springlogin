package com.example.springlogin.service;

import com.example.springlogin.controller.bean.User;

public interface UserService {

	User getUserByUserId(String userId);

	int createNewUser(User user);

}
