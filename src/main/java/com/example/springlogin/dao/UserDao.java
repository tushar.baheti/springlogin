package com.example.springlogin.dao;

import com.example.springlogin.controller.bean.User;

public interface UserDao {

	public User getUserById(String userId);

	int createNewUser(User user);

}
