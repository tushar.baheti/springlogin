package com.example.springlogin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.example.springlogin.controller.bean.User;
import com.example.springlogin.dao.UserDao;

//@Repository
//@Component("userDao")
@Repository
public class UserDaoImpl extends JdbcDaoSupport implements UserDao {

//	@Autowired
//	private JdbcTemplate jdbcTemplate;
//
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public User getUserById(String userId) {

//		System.out.println("userid is: ", userId);
//		String query = "select * from user where user_id=?;
		String query = "select * from users where user_id=?";

		return getJdbcTemplate().queryForObject(query, new Object[] { userId }, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User user = new User();
				user.setId(rs.getString(1));
				user.setUserId(userId);
				user.setPassword(rs.getString(3));

				System.out.println("UserDaoImpl user object " + user);

				return user;
			}

		});

//		User user = this.jdbcTemplate.queryForObject(query, new RowMapper<User>() {
//
//			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
//
//				User user = new User();
//				user.setUserId(userId);
//				user.setPassword(rs.getString(1));
//
//				return user;
//
//			}
//
//		}, userId);

//		return user;

	}

	@Override
	public int createNewUser(User user) {
		String query = "insert into users(id, user_id, password) values (default, ?, ?)";

		return getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement ps = con.prepareStatement(query);
				ps.setString(1, user.getUserId());
				ps.setString(2, user.getPassword());

				return ps;
			}
		});

//		int r = this.jdbcTemplate.update(query, user.getUserId(), user.getPassword());
//		return r;

	}

//	public JdbcTemplate getJdbcTemplate() {
//		return jdbcTemplate;
//	}
//
//	@Autowired
//	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
//		this.jdbcTemplate = jdbcTemplate;
//	}

}
