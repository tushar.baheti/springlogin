package com.example.springlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.example.springlogin")
public class SpringloginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringloginApplication.class, args);
	}

}
