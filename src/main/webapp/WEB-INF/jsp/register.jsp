<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
	
	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register page here</title>
<link href="webjars/bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">

<style>
.login-form{
width: 400px;
height: 200px;
top: 25%;
left: 25%;
position: absolute;
background-color: #ddd;
border-radius: 2px solid black;

}

.error{
	color: red;
	
}

</style>

</head>
<body>



<div class="login-form">

	
	<c:if test="${not empty errorMsg} ">
		<div>${errorMsg }</div>
	</c:if>
	
	

	
	<div class="container-fluid">
	
	<form:form method="post"  action="register" modelAttribute="user">
		<form:input type="text" path="userId" class="form-control mt-3" placeholder="Enter email address" /> 
		<form:input type="text" path="password" class="form-control mt-3" placeholder="Enter Password" /> <br>
		<form:errors path="userId" cssClass="error" /> <br>
		<form:button type="submit" class="btn btn-secondary  mt-3">Register</form:button>

	</form:form>
	
	
	
	</div>
	


</div>



</body>
</html>