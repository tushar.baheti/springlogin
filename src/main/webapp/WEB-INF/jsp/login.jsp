<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
	<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="webjars/bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">

<style>
.login-form{
width: 400px;
height: 300px;
top: 25%;
left: 25%;
position: absolute;
background-color: #ddd;
border-radius: 2px solid black;
padding: 20px;
}




</style>

</head>
<body>



<div class="login-form">

	<c:if test="${not empty errorMsg} ">
		<div>${errorMsg }</div>
	</c:if>
	
		<c:if test="${not empty successMsg} ">
		<div>${successMsg }</div>
	</c:if>

	
	<div class="container-fluid">
	
	<form method="post" action="login" style="padding: 20px;">
		<input type="text" name="userId" class="form-control mt-3" placeholder="Enter email address" /> 
		<input type="text" name="password" class="form-control mt-3" placeholder="Enter Password" /> <br>
	
	
	
		<button type="submit" class="form-control btn btn-primary  mt-3">Login</button>

		<div class="mt-3">
			<a href="/register" class="form-control btn btn-success mt-3">Register</a>
		</div>


	</form>
	
	
	
	</div>
	


</div>



</body>
</html>